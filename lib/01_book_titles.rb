class Book
  # TODO: your code goes here!
  attr_reader :title


  def title=(original)
    words = original.split(" ")
    new_words = []
    new_words << words.shift.capitalize
    words.each do |word|
      if %w(the a an in of for and).include?(word)
        new_words << word
      else
        new_words << word.capitalize
      end
    end

    @title = new_words.join(" ")
  end

end
