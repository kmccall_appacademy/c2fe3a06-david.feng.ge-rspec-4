class Timer

  def initialize
    @seconds = 0
  end

  attr_accessor :seconds



  def time_string
    time_str = ""
    hours = @seconds / 3600
    mins = ( @seconds % 3600 ) / 60
    secs = ( @seconds % 3600 ) % 60
    hours_s = convert(hours)
    mins_s = convert(mins)
    secs_s = convert(secs)
    "#{hours_s}:#{mins_s}:#{secs_s}"
  end

  private

  def convert(num)
    string = num.to_s
    return string if string.length == 2
    "0#{string}"
  end
  
end
