class Temperature
  # TODO: your code goes here!
  @fahrenheit
  @celsius

  attr_accessor :fahrenheit, :celsius

  def initialize(options = {})
    if options.include?(:f)
      @fahrenheit = options[:f]
      @celsius = (@fahrenheit - 32) * 5.0 / 9.0
    end
    if options.include?(:c)
      @celsius = options[:c]
      @fahrenheit = @celsius * 9.0 / 5.0 + 32
    end
  end

  def in_fahrenheit
    @fahrenheit
  end

  def in_celsius
    @celsius
  end

  def self.from_celsius(num)
    Temperature.new(:c => num)
  end

  def self.from_fahrenheit(num)
    Temperature.new(:f => num)
  end

end

class Celsius < Temperature
  def initialize(num)
    super(:c => num)
  end
end

class Fahrenheit < Temperature
  def initialize(num)
    super(:f => num)
  end
end
