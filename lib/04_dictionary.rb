class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def keywords
    @entries.keys.sort
  end

  def add(new_entry)
    if new_entry.class == Hash
      new_entry.each do |key, value|
        entries[key] = value
      end
    elsif new_entry.class == String
      entries[new_entry] = nil
    end
  end

  def include?(input)
    self.keywords.include?(input)
  end

  def printable
    output = ""
    keywords.each do |key|
      output << "[#{key}] \"#{entries[key]}\"\n"
    end
    output[0..-2]
  end

  def find(string)
    output = {}
    @entries.each do |key, value|
      if key.start_with?(string) # String#start_with!!
        output[key] = value
      end
    end
    output
  end

end
